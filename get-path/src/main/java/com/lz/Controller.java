package com.lz;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * @Author: lz
 * @Date: 2018/9/7 12:40
 * @Version 1.0
 */
@RestController
public class Controller {
	private static Properties properties = new Properties();
	private String property = System.getProperty("user.dir");

	@ModelAttribute
	public void init() throws Exception {
		System.out.println(property);
		properties.load(new FileReader(property + "/safe.properties"));

	}

	@GetMapping("/ip")
	public String getIp(@RequestParam String name,@RequestParam String password ,ServletRequest servletRequest) throws Exception {
		String uname = properties.getProperty("USERNAME");
		String pwd = properties.getProperty("PASSWORD");
		String shellName = properties.getProperty("SHELL.NAME");
		if (!uname.equals(name) || !pwd.equals(password)){
			return "密码错误";
		}

		String remoteAddr = servletRequest.getRemoteAddr();
		System.out.println( "ip:" + remoteAddr);

		String [] command ={"/bin/bash","-c",property+"/"+shellName +" "+remoteAddr};
		Runtime runTime = Runtime.getRuntime();
		int i = runTime.exec(command).waitFor();
		System.out.println(i);

		return remoteAddr + " SUCCESS" ;
	}

	public static void main(String[] args) {
		String property = System.getProperty("user.dir");
		System.out.println(property);
		File file = new File(property);
		File parentFile = file.getParentFile();
		System.out.println(parentFile);
	}
}
