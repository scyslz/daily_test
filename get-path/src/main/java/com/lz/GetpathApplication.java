package com.lz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetpathApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetpathApplication.class, args);
	}
}
