package cn.concurrent;

/**
 * @Author: lz
 * @Date: 2018/8/17 14:01
 * @Version 1.0
 */
public class JoinDemo {
	public static void main(String[] args) {
		Thread previousThread = Thread.currentThread();
		for (int i = 1; i <= 10; i++) {
			Thread curThread = new JoinThread(previousThread);
			curThread.start();
			previousThread = curThread;
		}
	}

	static class JoinThread extends Thread {
		private Thread thread;

		public JoinThread(Thread thread) {
			this.thread = thread;
		}

		@Override
		public void run() {
			try {
				//前一个线程thread执行完以后执行下一个
				thread.join();

				Thread.sleep(1);
				System.out.println(thread.getName() + " terminated.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

