package cn.concurrent.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Node节点查看Debug 啥也没看出来
 * @Author: lz
 * @Date: 2018/8/24 9:43
 * @Version 1.0
 */
public class LockDemo {
	private static ReentrantLock lock = new ReentrantLock();

	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			Thread thread = new Thread(() -> {
				lock.lock();
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					lock.unlock();
				}
			});
			thread.start();
		}
	}
}
