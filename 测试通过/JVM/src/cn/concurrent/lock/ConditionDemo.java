package cn.concurrent.lock;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: lz
 * @Date: 2018/8/24 16:26
 * @Version 1.0
 */
public class ConditionDemo {
	private static Lock lock = new ReentrantLock();
	private  static    Condition condition = lock.newCondition();

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			Thread thread = new Thread(() -> {
				lock.lock();
				try {
					condition.await();
					System.out.println(1);
					condition.signal();
					condition.signalAll();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}finally {
					lock.unlock();
				}
			});
			thread.start();
		}


	}
}
