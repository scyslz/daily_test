package cn.concurrent.lock;

import java.util.concurrent.locks.LockSupport;

/**
 * @Author: lz
 * @Date: 2018/8/27 11:42
 * @Version 1.0
 */
public class LockSupportDemo {
	public static void main(String[] args) {
		Thread thread = new Thread(()-> {
			// 底层UNSAFE.park
			LockSupport.park();
			System.out.println("被唤醒");
		});
		thread.start();

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// 唤醒thread线程
		LockSupport.unpark(thread);
	}
}
