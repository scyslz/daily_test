package cn.concurrent.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: lz
 * @Date: 2018/8/24 10:01
 * @Version 1.0
 */
public class SimpleDemo {
	private static Lock lock = new ReentrantLock();
	public static void main(String[] args) {
		lock.lock();
		System.out.println("hello");
		lock.unlock();


	}
}
