package cn.concurrent.container;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 原理复制一个数组用读写锁，锁住修改原数组的一个引用，数据实时性降低，占内存，不易阻塞
 * @Author: lz
 * @Date: 2018/8/27 17:12
 * @Version 1.0
 */
public class CopyOnWriteArrayListDemo {
	public static void main(String[] args) {
		CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
		for (int i=0 ;i<10;i++){
			copyOnWriteArrayList.add(i);

		}

		new Thread(()->{

			copyOnWriteArrayList.add(100);
		}).start();

		new Thread(()->{
			for (Object o : copyOnWriteArrayList) {
				System.out.println(o);

			}
		}).start();
	}
}
