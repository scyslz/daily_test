package cn.concurrent.container;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author: lz
 * @Date: 2018/8/27 13:49
 * @Version 1.0
 */
public class App implements Serializable {

	private static final int MAXIMUM_CAPACITY = 1 << 30;

	public static void main(String[] args) {
		// 1.
		ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap(32,0.7f,100);
		// 2.
		Hashtable hashtable = new Hashtable();
		// 3.
		HashMap hashMap = new HashMap();
		// 4.
		CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
		// 5.
		Vector vector =new Vector();
		// 6.Collection

		// 7. 队列的形式FIFO offer()移动tail节点，poll移动head节点
		ConcurrentLinkedQueue concurrentLinkedQueue=new ConcurrentLinkedQueue();
		// 8.
		ThreadLocal threadLocal = new ThreadLocal();
		ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
		reentrantReadWriteLock.writeLock();
		System.out.println(tableSizeFor(18));
		System.out.println(is2(1));
		returnTest();
	}

	/**
	 * 取最近的2的幂
	 * @param c
	 * @return
	 *
	 */
	private static final int tableSizeFor(int c) {
		int n = c - 1;
		n |= n >>> 1;
		n |= n >>> 2;
		n |= n >>> 4;
		n |= n >>> 8;
		n |= n >>> 16;
		return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
	}
	public static int is2(int n){

		return n&(n-1);
	}

	/**
	 * return 时50结束循环
	 */
	public static  void returnTest(){
		for (int i= 0; i<100;i++){
			if(i>50){
				return ;
			}
			System.out.println(i);
		}
	}
}
