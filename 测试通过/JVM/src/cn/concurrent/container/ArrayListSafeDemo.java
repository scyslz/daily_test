package cn.concurrent.container;

import java.util.ArrayList;
import java.util.List;

/**
 * java.util.ConcurrentModificationException
 *
 * @Author: lz
 * @Date: 2018/8/27 16:28
 * @Version 1.0
 */
public class ArrayListSafeDemo {
	public static void main(String[] args) {
		List list = new ArrayList();

		new Thread(() -> {
			for (int i = 0; i < 100; i++) {
				list.add(i);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();

		new Thread(() -> {
			if (list.size() > 0) {
				for (Object obj : list
						) {
					System.out.println(obj);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();


	}
}
