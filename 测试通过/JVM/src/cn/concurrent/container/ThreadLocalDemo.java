package cn.concurrent.container;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: lz
 * @Date: 2018/8/28 15:12
 * @Version 1.0
 */
public class ThreadLocalDemo {
	private static ThreadLocal sdf = new ThreadLocal<>();

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 100; i++) {
			executorService.submit(new DateUtil(""+ i ));
		}

	}

	static class DateUtil implements Runnable {
		private String date;

		public DateUtil(String date) {
			this.date = date;
		}

		@Override
		public void run() {
			if (sdf.get() == null) {
				sdf.set(date);
			} else {
				try {
					Object  date = sdf.get();
					System.out.println(Thread.currentThread().getName()+":"+date);
					sdf.remove();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}


}
