package cn.concurrent.utlis;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: lz
 * @Date: 2018/8/31 16:48
 * @Version 1.0
 */
public class CountDownLatchDemo {
	/**
	 * 举一个很通俗的例子，运动员进行跑步比赛时，假设有6个运动员参与比赛，
	 * 裁判员在终点会为这6个运动员分别计时，可以想象没当一个运动员到达终
	 * 点的时候，对于裁判员来说就少了一个计时任务。直到所有运动员都到达终
	 * 点了，裁判员的任务也才完成。这6个运动员可以类比成6个线程，当线程调
	 * 用CountDownLatch.countDown方法时就会对计数器的值减一，直到计数
	 * 器的值为0的时候，裁判员（调用await方法的线程）才能继续往下执行。
	 *
	 */

	private static CountDownLatch startSignal = new CountDownLatch(1);
	//用来表示裁判员需要维护的是6个运动员
	private static CountDownLatch endSignal = new CountDownLatch(6);

	public static void main(String[] args) throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(6);
		for (int i = 0; i < 6; i++) {
			executorService.execute(() -> {
				try {

						System.out.println(Thread.currentThread().getName() + " 运动员等待裁判员响哨！！！");
						startSignal.await();
						System.out.println(Thread.currentThread().getName() + "正在全力冲刺");
						endSignal.countDown();
						System.out.println(Thread.currentThread().getName() + "  到达终点");

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
		}
		Thread.sleep(100);
		System.out.println("裁判员发号施令啦！！！");
		startSignal.countDown();
		endSignal.await();
		System.out.println("所有运动员到达终点，比赛结束！");
		executorService.shutdown();
	}


}
