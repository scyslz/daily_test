package cn.concurrent.finaL;

/**
 * @Author: lz
 * @Date: 2018/8/22 14:08
 * @Version 1.0
 */
public class Main {
	public static void main(String[] args) {
		new Thread(() -> {
			FinalDemo.writer();
		},"write").start();
		new Thread(() -> {

			FinalDemo.reader();
		},"read").start();

	}
}
