package cn.concurrent.finaL;

/**
 * 类变量：必须要在静态初始化块中指定初始值或者声明该类变量时指定初始值，而且只能在这两个地方之一进行指定；
 * 实例变量：必要要在非静态初始化块，声明该实例变量或者在构造器中指定初始值，而且只能在这三个地方进行指定。
 * final局部变量 有且今有一次复制
 * final修饰对象时 只是实例对象的地址不能改变  其对象实例变量可以修改
 *
 * @Author: lz
 * @Date: 2018/8/22 11:34
 * @Version 1.0
 */
public class SimpleFinalDemo {
	// 第一种普通成员赋值方式
	private final int a = 10;
	private final int b;
	private final int d;

	//	第二种普通成员赋值
	{
		b = 10;
	}

	//第三种
	public SimpleFinalDemo() {
		d = 10;
	}


	// 类成员变量第一种
	private static final int c;

	// 类成员变量第二种
	static {

		c = 10;
	}

	class Parent {
		public final void method1() {
		}

	}

	class Child extends Parent {
//		不能被重写
//		@Override
//		public final void  method1(){}

		//可以被重载
		public final void method1(String Str) {
		}
	}

}
