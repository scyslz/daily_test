package cn.concurrent;

import java.util.concurrent.*;

/**
 * Excute线程执行Demo
 * @Author: lz
 * @Date: 2018/8/17 10:30
 * @Version 1.0
 */
public class ExecuteDemo {


	public static void main(String[] args) {
		// 创建线程池
		ExecutorService service = Executors.newSingleThreadExecutor();
		// 执行
		Future submit = service.submit(new MyThread());

		Executor executor = new Executor() {
			@Override
			public void execute(Runnable command) {
				System.out.println("我是execute");
				command.run();
			}

		};
		executor.execute(() -> {
			System.out.println("execute");
		});
	}

	static class MyThread implements Callable {

		@Override
		public Object call() throws Exception {

			System.out.println("callable");
			return null;
		}
	}

}
