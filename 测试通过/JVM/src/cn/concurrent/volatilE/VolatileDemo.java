package cn.concurrent.volatilE;

/**
 *  volatile关键字
 * @Author: lz
 * @Date: 2018/8/22 11:16
 * @Version 1.0
 */
public class VolatileDemo {
	private static volatile boolean isOver = false;
	//private static  boolean isOver = false;

	public static void main(String[] args) {
		Thread thread = new Thread(new Runnable() {
			int j = 0;
			@Override
			public void run() {
				while (!isOver) System.out.println(j++);;
			}
		});
		thread.start();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		isOver = true;
	}
}
