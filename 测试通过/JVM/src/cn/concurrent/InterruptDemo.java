package cn.concurrent;

import org.junit.Test;

/**
 * Interrupt详解
 *
 * @Author: lz
 * @Date: 2018/8/17 11:26
 * @Version 1.0
 */
public class InterruptDemo {
	/**
	 * interrupt并不是终止线程
	 * <p>
	 * interrupt() 向当前调用者线程发出中断信号
	 * <p>
	 * isinterrupted() 查看当前中断信号是true还是false
	 * <p>
	 * interrupted() 是静态方法，查看当前中断信号是true还是false并且清除中断信号，顾名思义interrupted为已经处理中断信号。
	 * <p>
	 * 注：interrupt()方法发出的中断信号只能被wait() sleep() join()这三个方法捕捉到并产生中断。（目前我所知）
	 *
	 * @param args
	 *
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Thread t = new Thread(new Worker());
		t.start();

		Thread.sleep(200);   //不影响
		t.interrupt();            //发出中段信号

		System.out.println("Main thread stopped.");
	}

	public static class Worker implements Runnable {
		@Override
		public void run() {
			System.out.println("Worker started.");

			try {
				//注：interrupt()方法发出的中断信号只能被wait() sleep() join()这三个方法捕捉到并产生中断。（目前我所知），捕获异常清除终端标记
				Thread.sleep(500);
			} catch (InterruptedException e) {
				Thread curr = Thread.currentThread();
				//再次调用interrupt方法中断自己，将中断状态设置为“中断”
				curr.interrupt();

				System.out.println("Worker IsInterrupted: " + curr.isInterrupted());         //true
				System.out.println("Worker IsInterrupted: " + curr.isInterrupted());
				System.out.println("Static Call: " + Thread.interrupted());                  //clear status 查看中断标志True，清除中断标记
				System.out.println("---------After Interrupt Status Cleared----------");
				System.out.println("Static Call: " + Thread.interrupted());                  //此时中断已经被清除
				System.out.println("Worker IsInterrupted: " + curr.isInterrupted());
				System.out.println("Worker IsInterrupted: " + curr.isInterrupted());
			}

			System.out.println("Worker stopped.");
			/*	Worker started.
			Main thread stopped.
			Worker IsInterrupted: true
			Worker IsInterrupted: true
			Static Call: true
					---------After Interrupt Status Cleared----------
			Static Call: false
			Worker IsInterrupted: false
			Worker IsInterrupted: false
			Worker stopped.*/


		}
	}
	@Test
	public  void Demo2 () throws InterruptedException {
		//sleepThread睡眠1000ms
		final Thread sleepThread = new Thread() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				super.run();
			}
		};
		//busyThread一直执行死循环
		Thread busyThread = new Thread() {
			@Override
			public void run() {
				while (true) ;
			}
		};
		sleepThread.start();
		busyThread.start();
		sleepThread.interrupt();
		busyThread.interrupt();
		int i =0;		while (sleepThread.isInterrupted()) i++;
		//sleepThread线程抛出interrupt的时间不一样
		System.out.println(i);

		System.out.println("sleepThread isInterrupted: " + sleepThread.isInterrupted());
		System.out.println("busyThread isInterrupted: " + busyThread.isInterrupted());
		/**
		 * 开启了两个线程分别为sleepThread和BusyThread, sleepThread睡眠1s，BusyThread执行死循环。然后分别对着两个线程进行中断操作，
		 *
		 * 可以看出sleepThread抛出InterruptedException后清除标志位，而busyThread就不会清除标志位。
		 *
		 */
	}



}
