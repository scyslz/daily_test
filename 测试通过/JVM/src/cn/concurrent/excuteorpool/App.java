package cn.concurrent.excuteorpool;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: lz
 * @Date: 2018/8/29 11:44
 * @Version 1.0
 */
public class App {

	public static void main(String[] args) {
//		TimeUnit days = TimeUnit.DAYS;
//		ThreadPoolExecutor threadPoolExecutor  = new ThreadPoolExecutor(days);
//		Execute();
	//	ExecuteService();
		Integer r =  -1 << (Integer.SIZE-3);//-536870912    -2^29
		Integer s =  0 << (Integer.SIZE-3); //0
		Integer stop =  1 << (Integer.SIZE-3);// 536870912 2^30
		Integer stidying =  2 << (Integer.SIZE-3);      //2^30
		System.out.println(r+":"+s+":"+stop+":"+stidying);
	}

	private static void ExecuteService() {
		ExecutorService executorService = Executors.newFixedThreadPool(20);
	}

	private static void Execute() {
		Executor executor = new Executor() {
			@Override
			public void execute(Runnable command) {
				System.out.println("我是excute方法"+command);
				command.run();
			}
		};
		Thread thread = new Thread(()->{
			System.out.println("我是thread中的run");
		});
		executor.execute(thread);
	}
}
