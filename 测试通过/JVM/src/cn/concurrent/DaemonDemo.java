package cn.concurrent;

/**
 * Daemon 守护线程
 *
 * @Author: lz
 * @Date: 2018/8/17 14:17
 * @Version 1.0
 */
public class DaemonDemo {
	public static void main(String[] args) {
		Thread daemonThread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						System.out.println("i am alive");
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						System.out.println("finally block");
					}
				}
			}
		});
		//如果 User Thread已经全部退出运行了，只剩下Daemon Thread存在了，虚拟机也就退出了。 因为没有了被守护者，Daemon也就没有工作可做了，也就没有继续运行程序的必要了。
		//此处daemon是守护线程 user 是main

//		(1) thread.setDaemon(true)必须在thread.start()之前设置，否则会跑出一个IllegalThreadStateException异常。你不能把正在运行的常规线程设置为守护线程。
//		(2) 在Daemon线程中产生的新线程也是Daemon的。
//		(3) 不要认为所有的应用都可以分配给Daemon来进行服务，比如读写操作或者计算逻辑。
		daemonThread.setDaemon(true);
		daemonThread.start();

		//确保main线程结束前能给daemonThread能够分到时间片
		try {
			Thread.sleep(1800);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
