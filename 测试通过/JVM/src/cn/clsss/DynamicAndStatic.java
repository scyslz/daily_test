package cn.clsss;

/**
 *  静态分派 和 动态分派
 * @Author: lz
 * @Date: 2018/8/22 16:23
 * @Version 1.0
 */
public class DynamicAndStatic {
	static  class parent{
		void dothing  (){
			System.out.println("parent");
		};
	}
	static  class Child1 extends   parent{

		@Override
		public void dothing() {
			System.out.println("child1");
		}
	}
	static  class Child2 extends   parent{

		@Override
		public void dothing() {
			System.out.println("child2");
		}
	}


	static class StaticPai{

		public void say(parent parent){
			System.out.println("I am parent");
		}
		public void say(Child1 child1){
			System.out.println("I am child1");
		}
		public void say(Child2 child2){
			System.out.println("I am child2");
		}


	}
	public static void main(String[] args) {
	// 静态分派
		StaticPai staticPai = new StaticPai();
		parent p =new Child1();
		Child2 child2 = new Child2();
		staticPai.say(p);
		staticPai.say(child2);
		//动态分派
		p.dothing();


	}
}
