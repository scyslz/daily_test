package cn.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @Author: lz
 * @Date: 2018/8/16 11:22
 * @Version 1.0
 */
public class AutoSearchAnswerExcepetionUtls {
	public static void main(String[] args) {
		try {
			t1();
		} catch (Exception e) {
			// 根据不同的异常通过overflows
			String url = "https://stackoverflow.com/search?q=" + e.getMessage();
			//requestUrl(url);
			openURL(url);
		}
	}

	private static void t1() {
		int j = 5 / 0;
	}

	/**
	 * 返回真个网页的内容
	 *
	 * @param urlKeyWords
	 */
	public static void requestUrl(String urlKeyWords) {
		try {
			//建立连接
			URL url = new URL(urlKeyWords);
			HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
			httpUrlConn.setDoInput(true);
			httpUrlConn.setRequestMethod("GET");
			httpUrlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
			//获取输入流
			InputStream input = httpUrlConn.getInputStream();
			//将字节输入流转换为字符输入流
			InputStreamReader read = new InputStreamReader(input, "utf-8");
			//为字符输入流添加缓冲
			BufferedReader br = new BufferedReader(read);
			// 读取返回结果
			String data = br.readLine();
			while (data != null) {
				System.out.println(data);
				data = br.readLine();
			}
			// 释放资源
			br.close();
			read.close();
			input.close();
			httpUrlConn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * open
	 *
	 * @param url
	 */
	public static void openURL(String url) {
		try {
			browse(url);
		} catch (Exception e) {
		}
	}

	/**
	 * 选择浏览器并打开url
	 *
	 * @param url
	 *
	 * @throws Exception
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	private static void browse(String url) throws Exception {
		//获取操作系统的名字
		String osName = System.getProperty("os.name", "");
		if (osName.startsWith("Mac OS")) {
			//苹果的打开方式
			Class fileMgr = Class.forName("com.apple.eio.FileManager");
			Method openURL = fileMgr.getDeclaredMethod("openURL", new Class[]{String.class});
			openURL.invoke(null, new Object[]{url});
		} else if (osName.startsWith("Windows")) {
			//windows的打开方式。
            /*String browspath = System.getProperty("brows.path");
            try {
                if(browspath != null){
                    Runtime.getRuntime().exec("browspath " + url);
                }
                //Runtime.getRuntime().exec("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe " + url);
            } catch (Exception e) {
                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
            }
            */
			Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);

		} else {
			// Unix or Linux的打开方式
			String[] browsers = {"firefox", "opera", "konqueror", "epiphany", "mozilla", "netscape"};
			String browser = null;
			for (int count = 0; count < browsers.length && browser == null; count++)
				//执行代码，在brower有值后跳出，
				//这里是如果进程创建成功了，==0是表示正常结束。
				if (Runtime.getRuntime().exec(new String[]{"which", browsers[count]}).waitFor() == 0)
					browser = browsers[count];
			if (browser == null)
				throw new Exception("Could not find web browser");
			else
				//这个值在上面已经成功的得到了一个进程。
				Runtime.getRuntime().exec(new String[]{browser, url});
		}
	}
}
