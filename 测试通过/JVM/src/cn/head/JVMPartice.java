package cn.head;

/**
 * @Author: lz
 * @Date: 2018/8/16 16:13
 * @Version 1.0
 */
public class JVMPartice {
	/**
	 * -Xmx12M -XX:+PrintGCDetails -XX:+PrintGCTimeStamps
	 * ----result---
	 *
	 * 0.147: [GC (Allocation Failure) [PSYoungGen: 2574K->504K(3584K)] 2574K->838K(11776K), 0.0023888 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
	 * 0.150: [GC (Allocation Failure) [PSYoungGen: 504K->504K(3584K)] 838K->878K(11776K), 0.0100725 secs] [Times: user=0.05 sys=0.00, real=0.01 secs]
	 * 0.160: [Full GC (Allocation Failure) [PSYoungGen: 504K->0K(3584K)] [ParOldGen: 374K->800K(5632K)] 878K->800K(9216K), [Metaspace: 3683K->3683K(1056768K)], 0.0049350 secs] [Times: user=0.00 sys=0.00, real=0.01 secs]
	 * 0.165: [GC (Allocation Failure) [PSYoungGen: 0K->0K(3584K)] 800K->800K(11776K), 0.0003184 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
	 * 0.165: [Full GC (Allocation Failure) [PSYoungGen: 0K->0K(3584K)] [ParOldGen: 800K->764K(7680K)] 800K->764K(11264K), [Metaspace: 3683K->3683K(1056768K)], 0.0155029 secs] [Times: user=0.05 sys=0.00, real=0.02 secs]
	 * Heap
	 *  PSYoungGen      total 3584K, used 87K [0x00000000ffc00000, 0x0000000100000000, 0x0000000100000000)
	 *   eden space 3072K, 2% used [0x00000000ffc00000,0x00000000ffc15da0,0x00000000fff00000)
	 *   from space 512K, 0% used [0x00000000fff00000,0x00000000fff00000,0x00000000fff80000)
	 *   to   space 512K, 0% used [0x00000000fff80000,0x00000000fff80000,0x0000000100000000)
	 *  ParOldGen       total 8192K, used 764K [0x00000000ff400000, 0x00000000ffc00000, 0x00000000ffc00000)
	 *   object space 8192K, 9% used [0x00000000ff400000,0x00000000ff4bf3a0,0x00000000ffc00000)
	 *  Metaspace       used 3714K, capacity 4672K, committed 4864K, reserved 1056768K
	 *   class space    used 411K, capacity 432K, committed 512K, reserved 1048576K
	 * @param args
	 */
	public static void main(String[] args) {
		MyJvmMemoryOutErrors jvmParmarsTest = new MyJvmMemoryOutErrors();
		jvmParmarsTest.objectOutHeadSpace();
	}
}
