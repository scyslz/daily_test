package cn.collections;

import java.util.*;

/**
 * @Author: lz
 * @Date: 2018/8/22 17:01
 * @Version 1.0
 */
public class MapAndSet {
	/**
	 * JDK8 以后set实现做了调整多次运算看似有序，不保证有序!=保证无序 hashmap不关心是否有序
	 * @param args
	 */
	public static void main(String[] args) {

		Map<String,String> map = new HashMap();
		map.put("1","a");
		map.put("2","b");
		map.put("3","c");
		Set<String> set =new HashSet();
		set.add("a");
		set.add("b");
		set.add("c");

		Iterator<Map.Entry<String, String>> iterator =map.entrySet().iterator();
		while(iterator.hasNext()){
			Map.Entry<String, String> next = iterator.next();
			System.out.println(next.getKey()+""+next.getValue());
		}
		// jdk8 性特性
		map.forEach((k,y)->{
			System.out.println(k+":"+y);
		});
		Iterator<String> iterator1 = set.iterator();
		while (iterator1.hasNext()){

			String next = iterator1.next();
			System.out.println(next);
		}
	}
}
