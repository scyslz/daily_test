package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * @Author: lz
 * @Date: 2018/8/21 16:23
 * @Version 1.0
 */
@Configuration
public class Routes {

	@Autowired
	private UserHandler userHandler;

//	public Routes(UserHandler userHandler) {
//		this.userHandler = userHandler;
//	}

	@Bean
	public RouterFunction<?> routerFunction() {
		return route(GET("/api/user").and(accept(MediaType.APPLICATION_JSON)), userHandler::handleGetUsers)
				.and(route(GET("/api/user/{id}").and(accept(MediaType.APPLICATION_JSON)), userHandler::handleGetUserById))
				.and(route(GET("/hello").and(accept(MediaType.APPLICATION_JSON)), userHandler::handleHello));
	}

}