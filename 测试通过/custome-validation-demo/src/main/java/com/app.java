package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: lz
 * @Date: 2018/8/30 9:05
 * @Version 1.0
 */
@SpringBootApplication
public class app {
	public static void main(String[] args) {
		SpringApplication.run(app.class);
	}
}
