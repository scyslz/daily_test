package com.model;

import com.validation.CustomValidation;

import javax.validation.constraints.NotNull;

/**
 * @Author: lz
 * @Date: 2018/8/30 9:01
 * @Version 1.0
 */
public class User {
	@NotNull
	private String name;
	@CustomValidation
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
