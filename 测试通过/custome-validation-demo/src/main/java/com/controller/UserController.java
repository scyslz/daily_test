package com.controller;

import com.model.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Author: lz
 * @Date: 2018/8/30 9:03
 * @Version 1.0
 */
@RestController
public class UserController {
	@PostMapping("/save")
	public User save(@RequestBody @Valid User user) {
		return user;
	}
}
