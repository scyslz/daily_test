package com.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author: lz
 * @Date: 2018/8/30 9:13
 * @Version 1.0
 */
public class CustomerValidator implements ConstraintValidator<CustomValidation, String> {
	@Override
	public void initialize(CustomValidation constraintAnnotation) {

	}

	/**
	 * 校验只能是数字和字母不能为空
	 *
	 * @param value
	 * @param context
	 *
	 * @return
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		String regu = "^[0-9a-zA-Z_]*";

		boolean matches = value.matches(regu);
		return matches;
	}
}
