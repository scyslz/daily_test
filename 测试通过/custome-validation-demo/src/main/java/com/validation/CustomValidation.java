package com.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @Author: lz
 * @Date: 2018/8/30 9:01
 * @Version 1.0
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {CustomerValidator.class})

public @interface CustomValidation {
	String message() default "{custom.valid.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
