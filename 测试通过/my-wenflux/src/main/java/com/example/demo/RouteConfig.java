package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;

//两个静态方法ALT+/不能导入，只有ALT+ENTER提示
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;


/**
 * @Author: lz
 * @Date: 2018/8/27 9:39
 * @Version 1.0
 */
@Configuration
public class RouteConfig {
	@Bean
	public RouterFunction hello(Handler handler) {
		return route(GET("/hello"), handler::hello);
	}
}
