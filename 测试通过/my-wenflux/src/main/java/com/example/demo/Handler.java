package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
//两个静态方法ALT+/不能导入，只有ALT+ENTER提示
import static org.springframework.web.reactive.function.BodyInserters.*;

/**
 * @Author: lz
 * @Date: 2018/8/27 9:40
 * @Version 1.0
 */
@Component
public class Handler {
	public Mono<ServerResponse> hello(ServerRequest serverRequest) {
		Mono<String> mono = Mono.just("Hello");
		return mono.flatMap(strMono -> ServerResponse.ok().contentType(MediaType.TEXT_PLAIN).body(fromObject(strMono)));
	}
}
