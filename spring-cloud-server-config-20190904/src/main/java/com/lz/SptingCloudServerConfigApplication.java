package com.lz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SptingCloudServerConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(SptingCloudServerConfigApplication.class, args);
	}
}
