### 第1章 简介

#### 1.1 上下文切换

即使是单核处理器也支持多线程执行代码，CPU通过给每个线程分配CPU时间片来实现这个机制。时间片是CPU分配给各个线程的时间，因为时间片非常短，所以CPU通过不停地切换线程执行，让我们感觉多个线程是同时执行的，时间片一般是几十毫秒（ms）。

> 在切换前会保存上一个任务的状态，以便下次切换回这个任务时，可以再加载这个任务的状态。所以任务从保存到再加载的过程就是一次上下文切换。

```java
public class ConcurrencyTest {
	// 当为百万时约等
	private static final long count = 1000000L;
	public static void main(String[] args) throws InterruptedException {
		concurrency();
		serial();
	}
	private static void concurrency() throws InterruptedException {
		long start = System.currentTimeMillis();
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				int a = 0;
				for (long i = 0; i < count; i++) {
					a += 5;
				}
			}
		});
		thread.start();
		int b = 0;
		for (long i = 0; i < count; i++) {
			b--;
		}
		long time = System.currentTimeMillis() - start;
		thread.join();
		System.out.println("concurrency :" + time+"ms,b="+b);
	}
	private static void serial() {
		long start = System.currentTimeMillis();
		int a = 0;
		for (long i = 0; i < count; i++) {
			a += 5;
		}
		int b = 0;
		for (long i = 0; i < count; i++) {
			b--;
		}
		long time = System.currentTimeMillis() - start;
		System.out.println("serial:" + time+"ms,b="+b+",a="+a);
	}
}
```

#### 1.2 减少上下文切换

减少上下文切换的方法有无锁并发编程、CAS算法、使用最少线程和使用协程。

> ·协程：在单线程里实现多任务的调度，并在单线程里维持多个任务间的切换。

#### 1.3 避免死锁的几个常见方法
> 避免一个线程同时获取多个锁。
> 避免一个线程在锁内同时占用多个资源，尽量保证每个锁只占用一个资源。
> 尝试使用定时锁，使用lock.tryLock（timeout）来替代使用内部锁机制。
> 对于数据库锁，加锁和解锁必须在一个数据库连接里，否则会出现解锁失败的情况。

#### 1.4 资源限制的挑战
    在并发编程中，将代码执行速度加快的原则是将代码中串行执行的部分变成并发执行，但是如果将某段串行的代码并发执行，因为受限于资源，仍然在串行执行，这时候程序不仅不会加快执行，反而会更慢，因为增加了上下文切换和资源调度的时间。

> 对于硬件资源限制，可以考虑使用集群并行执行程序。
>
> 对于软件资源限制，可以考虑使用资源池将资源复用。比如使用连接池将数据库Socket连接复用，或者在调用对方webservice接口获取数据时，只建立一个连接。
>
> 根据不同的资源限制调整程序的并发度，比如下载文件程序依赖于两个资源——带宽和硬盘读写速度。有数据库操作时，涉及数据库连接数，如果SQL语句执行非常快，而线程的数量比数据库连接数大很多，则某些线程会被阻塞，等待数据库连接。
>
> 建议多使用JDK并发包提供的并发容器和工具类来解决并发
> 问题.



### 第2章 Java并发机制的底层实现原理
    Java代码在编译后会变成Java字节码，字节码被类加载器加载到JVM里，JVM执行字节码，最终需要转化为汇编指令在CPU上执行，Java中所使用的并发机制依赖于JVM的实现和CPU的指令。

#### 2.1 volatile的应用

在多线程并发编程中synchronized和volatile都扮演着重要的角色，volatile是轻量级的synchronized，它在多处理器开发中保证了共享变量的“可见性”。如果volatile变量修饰符使用恰当的话，它比synchronized的使用和执行成本更低。

```
Java代码如下。
instance = new Singleton();                 // instance是volatile变量
转变成汇编代码，如下。
0x01a3de1d: movb $0×0,0×1104800(%esi);0x01a3de24: lock addl $0×0,(%esp);
```

> 1）将当前处理器缓存行的数据写回到系统内存。
> 2）这个写回内存的操作会使在其他CPU里缓存了该内存地址的数据无效。

为了提高处理速度，处理器不直接和内存进行通信，而是先将系统内存的数据读到内部缓存（L1，L2或其他）后再进行操作，但操作完不知道何时会写到内存。如果对声明了volatile的变量进行写操作，JVM就会向处理器发送一条**Lock前缀的指令**，将这个变量所在缓存行的数据写回到系统内存。但是，就算写回到内存，如果其他处理器缓存的值还是旧的，再执行计算操作就会有问题。所以，在多处理器下，为了保证各个处理器的缓存是一致的，就会实现**缓存一致性协议**，每个处理器通过**嗅探在总线上传播的数据**来检查自己缓存的值是不是过期了，当处理器发现自己缓存行对应的内存地址被修改，就会将当前处理器的缓存行设置成无效状态，当处理器对这个数据进行修改操作的时候，会重新从系统内存中把数据读到处理器缓存里

#### 2.2 volatile的两条实现原则

> 1. Lock前缀指令会引起处理器缓存回写到内存。

Lock前缀指令导致在执行指令期间，处理器的LOCK#信号。在多处理器环境中，LOCK#信号确保在声言该信号期间，处理器可以***独占任何共享内存***。LOCK＃信号***一般不锁总线，而是锁缓存，毕竟锁总线开销的比较大***。

在8.1.4节有详细说明锁定操作对处理器缓存的影响，对于Intel486和Pentium处理器，在**锁操作时，总是在总线上声言LOCK#信号**。

但在P6和目前的处理器中，如果**访问的内存区域已经缓存在处理器内部**，则不会声言LOCK#信号。

相反，它会**锁定这块内存区域的缓存并回写到内存，并使用缓存一致性机制来确保修改的原子性**，此操作被称为“缓存锁定”，缓存一致性机制会阻止同时修改由两个以上处理器缓存的内存区域数据。

> 2. 一个处理器的缓存回写到内存会导致其他处理器的缓存无效

 MESI（修改、独占、共享、无效）控制协议去维护内部缓存和其他处理器缓存的一致性；多核处理器，IA-32和Intel 64处理器能**嗅探其他处理器访问系统内存和它们的内部缓存**。处理器使嗅探技术保证它的内部缓存、系统内存和其他处理器的缓存的数据在总线上保持一致。

例如，在Pentium和P6 family处理器中，如果通过嗅探一个处理器来检测其他处理器打算写内存地址，而这个地址当前处于共享状态，那么正在嗅探的处理器将使它的缓存行无效，在下次访问相同内存地址时，强制执行缓存行填充。

####2.3 synchronized的实现原理与应用

> ·对于普通同步方法，锁是当前实例对象。
> ·对于静态同步方法，锁是当前类的Class对象。
> ·对于同步方法块，锁是Synchonized括号里配置的对象。

当一个线程试图访问同步代码块时，它首先必须得到锁，退出或抛出异常时必须释放锁。那么锁到底存在哪里呢？锁里面会存储什么信息呢？

Synchonized在JVM里的实现原理，JVM基于进入和退出Monitor对象来实现方法同步和代码块同步，但两者的实现细节不一样。代码块同步是使用monitorenter和monitorexit指令实现的，而方法同步是使用另外一种方式实现的，细节在JVM规范里并没有详细说明。但是，方法的同步同样可以使用这两个指令来实现。

monitorenter指令是在编译后插入到同步代码块的开始位置，而monitorexit是插入到方法结束处和异常处，JVM要保证每个monitorenter必须有对应的monitorexit与之配对。**任何对象都有一个monitor与之关联，当且一个monitor被持有后，它将处于锁定状态**。线程执行到monitorenter指令时，将会尝试获取对象所对应的monitor的所有权，即尝试获得对象的锁.

而这个获取的过程是**互斥**的，即同一时刻只有一个线程能够获取到monitor如果没有获取到监视器的线程将会被阻塞在同步块和同步方法的入口处，进入到**BLOCKED状态**。

```java
public class SynchronizedDemo {
    public static void main(String[] args) {
        synchronized (SynchronizedDemo.class) {
        }
        method();
    }
    private static void method() {
    }
}
```

字节码文件，详见Synchronize.jpg

执行静态同步方法的时候就只有一条monitorexit指令，并没有monitorenter获取锁的指令。这就是**锁的重入性**，即在同一锁程中，线程不需要再次获取同一把锁。Synchronized先天具有重入性。**每个对象拥有一个计数器，当线程获取该对象锁后，计数器就会加一，释放锁后就会将计数器减一**。

#### 2.4 CAS操作

CAS实现类有很多，可以说是支撑起整个concurrency包的实现，在Lock实现中会有CAS改变state变量，在atomic包中的实现类也几乎都是用CAS实现

CAS的问题

> 1. ABA问题
> 2. 无限自旋
> 3. 只能保证一个共享变量的原子操作

#### 2.5 Java对象头

详见Java对象头表

#### 2.6 synchronize优化

> 无锁状态 、偏向锁状态 、 轻量级锁状态和重量级锁状态

锁一共有4种状态，这几个状态会随着竞争情况逐渐升级。锁可以升级但不能降级，意味着偏向锁升级成轻量级锁后不能降级成偏向锁。这种锁升级却不能降级的策略，目的是为了提高获得锁和释放锁的效率

**偏向锁**

引入偏向锁是为了在无多线程竞争的情况下尽量减少不必要的轻量级锁执行路径，因为轻量级锁的获取及释放依赖多次CAS原子指令，而偏向锁只需要在置换ThreadID的时候依赖一次CAS原子指令（由于一旦出现多线程竞争的情况就必须撤销偏向锁，所以偏向锁的撤销操作的性能损耗必须小于节省下来的CAS原子指令的性能消耗）。上面说过，轻量级锁是为了在线程交替执行同步块时提高性能，而偏向锁则是在只有一个线程执行同步块时进一步提高性能。

**1、偏向锁获取过程：**

> （1）访问Mark Word中偏向锁的标识是否设置成1，锁标志位是否为01——确认为可偏向状态。
>
> （2）如果为可偏向状态，则测试线程ID是否指向当前线程，如果是，进入步骤（5），否则进入步骤（3）。
>
> （3）如果线程ID并未指向当前线程，则通过CAS操作竞争锁。如果竞争成功，则将Mark Word中线程ID设置为当前线程ID，然后执行（5）；如果竞争失败，执行（4）。
>
> （4）如果CAS获取偏向锁失败，则表示有竞争。当到达全局安全点（safepoint）时获得偏向锁的线程被挂起，偏向锁升级为轻量级锁，然后被阻塞在安全点的线程继续往下执行同步代码。
>
> （5）执行同步代码。

**2、偏向锁的释放：**

> 偏向锁只有遇到其他线程尝试竞争偏向锁时，持有偏向锁的线程才会释放锁，线程**不会主动去释放偏向锁**。偏向锁的撤销，需要等待全局安全点（在这个时间点上没有字节码正在执行），它会首先暂停拥有偏向锁的线程，判断锁对象是否处于被锁定状态，撤销偏向锁后恢复到**未锁定（标志位为“01”）或轻量级锁（标志位为“00”）**的状态。

**轻量级锁**

**1、轻量级锁的加锁过程**

> （1）在代码进入同步块的时候，如果同步对象锁状态为无锁状态（锁标志位为“01”状态，是否为偏向锁为“0”），虚拟机首先将在当前线程的栈帧中建立一个名为锁记录（Lock Record）的空间，用于存储锁对象目前的Mark Word的拷贝，官方称之为 Displaced Mark Word。这时候线程堆栈与对象头的状态如图2.1所示。
>
> （2）拷贝对象头中的Mark Word复制到锁记录中。
>
> （3）拷贝成功后，虚拟机将使用CAS操作尝试将对象的Mark Word更新为指向Lock Record的指针，并将Lock record里的owner指针指向object mark word。如果更新成功，则执行步骤（4），否则执行步骤（5）。
>
> （4）如果这个更新动作成功了，那么这个线程就拥有了该对象的锁，并且对象Mark Word的锁标志位设置为“00”，即表示此对象处于轻量级锁定状态，这时候线程堆栈与对象头的状态如图2.2所示。
>
> （5）如果这个更新操作失败了，虚拟机首先会检查对象的Mark Word是否指向当前线程的栈帧，如果是就说明当前线程已经拥有了这个对象的锁，那就可以直接进入同步块继续执行。否则说明多个线程竞争锁，轻量级锁就要膨胀为重量级锁，锁标志的状态值变为“10”，Mark Word中存储的就是指向重量级锁（互斥量）的指针，后面等待锁的线程也要进入阻塞状态。 而当前线程便尝试使用自旋来获取锁，自旋就是为了不让线程阻塞，而采用循环去获取锁的过程。

**2、轻量级锁释放**

> （1）通过CAS操作尝试把线程中复制的Displaced Mark Word对象替换当前的Mark Word。
>
> （2）如果替换成功，整个同步过程就完成了。
>
> （3）如果替换失败，说明有其他线程尝试过获取该锁（此时锁已膨胀），那就要在释放锁的同时，唤醒被挂起的线程。

案例

锁升级场景:蜀国四兄弟争骑单车007号
1、 # 获取偏向锁
张飞要骑

                有人吗
    
                                没人
    
                                        写上张飞的名字
    
                                                    成功获取偏向锁                     
    
                                                                  骑走

1.1、# 拥有偏向锁
张飞要骑车

                  有我的名字
    
                                骑走

2、#争抢进入轻量级锁
刘备要骑

            看到张飞的名字在，问张飞在吗
    
                            张飞：在
    
                                       尝试写上刘备的名字失败
    
                                                  张飞我在等你
    
                                                              刘备骑完后把他的名字抹去进入轻量级锁

2.1、#争抢获取偏向锁
                                      尝试写上刘备名字成功

                                                  获取偏向锁
    
                                                            骑走

3、#争抢轻量级锁
诸葛亮要骑

        拍照单车挂身上（复制对象头锁信息到线程栈内存）
    
                  尝试在单车箭头指向自己照片（对象头锁信息指针指向线程栈内存）
    
                            成功
    
                                    成功获取轻量级锁
    
                                              骑走
    
                                                        还车成功

3.1、#已经争抢成了重量级锁
                                                        还车失败，好了好了你们快抢（关羽）

4、#争抢轻量级锁
关羽要骑车

          拍照单车
    
                  尝试在单车箭头指向自己照片
    
                            失败
    
                                  再试多次（自旋锁）
    
                                          艹有完没完了，设置重量级锁
    
                                                      蹲等
    
                                                              诸葛亮说骑完了，抢

#### 2.7原子操作

> 1.使用总线锁保证原子性

处理器提供的一个LOCK＃信号，当一个处理器在总线上输出此信号时，其他处理器的请求将被阻塞住，那么该
处理器可以独占共享内存.

> 2.使用缓存锁保证原子性

通过缓存通信

优先考虑2，特定情况用1

